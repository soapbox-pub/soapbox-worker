const path = require('path');

const webpack = require('webpack');

const package = require('./package.json');

/** Get the git commit hash for the current commit. */
// https://stackoverflow.com/a/38401256
const getCommitHash = () => {
  return require('child_process').execSync('git rev-parse --short HEAD').toString().trim();
};

module.exports = {
  entry: './src/index.ts',
  output: {
    filename: 'worker.js',
    path: path.join(__dirname, 'dist'),
  },
  devtool: 'cheap-module-source-map',
  mode: 'development',
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          // transpileOnly is useful to skip typescript checks occasionally:
          // transpileOnly: true,
        },
      },
    ],
  },
  plugins: [
    new webpack.BannerPlugin({
      banner: [
        package.displayName,
        package.repository.url,
        `Version: v${package.version}-${getCommitHash()}`,
      ].join('\n'),
    }),
  ],
};
