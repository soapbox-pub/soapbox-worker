import fs from 'fs';

import { handleEvent } from '../src';

/** Create a FetchEvent for testing. */
// https://stackoverflow.com/a/71809806
const createEvent = (request: Request): FetchEvent => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return new FetchEvent('fetch', { request });
};

describe('handleEvent', () => {
  beforeAll(() => {
    // https://github.com/cloudflare/miniflare/issues/162#issuecomment-1059549646
    const fetchSpy = jest.spyOn(globalThis, 'fetch');

    fetchSpy.mockImplementation(async (input, init) => {
      const req = new Request(input, init);
      const url = new URL(req.url);

      switch (url.pathname) {
        case '/api/v1/accounts/lookup?acct=alex':
          return new Response(fs.readFileSync('test/fixtures/account.json', 'utf8'));
        case '/api/v1/statuses/123':
          return new Response(fs.readFileSync('test/fixtures/status.json', 'utf8'));
        case '/api/v1/groups/109989480368015378':
          return new Response(fs.readFileSync('test/fixtures/group-truthsocial.json', 'utf8'));
        case '/api/v1/groups/lookup?name=patriot-patriots':
          return new Response(fs.readFileSync('test/fixtures/group-truthsocial.json', 'utf8'));
        case '/api/v1/instance':
          return new Response(fs.readFileSync('test/fixtures/instance.json', 'utf8'));
        default:
          return new Response(fs.readFileSync('test/fixtures/static/index.html', 'utf8'), {
            headers: { 'content-type': 'text/html' },
          });
      }
    });
  });

  test('handle GET /', async () => {
    const event = createEvent(new Request('https://gleasonator.com'));
    const result = await handleEvent(event);
    expect(result.status).toEqual(200);
    const text = await result.text();
    expect(text).toContain('<!DOCTYPE html>');
  });
});
