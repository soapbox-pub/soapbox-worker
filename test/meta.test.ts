import { buildHtml } from '../src/meta';

import account from './fixtures/account.json';
import instance from './fixtures/instance.json';
import status from './fixtures/status.json';
import statusWithImage from './fixtures/status-with-image.json';
import statusWithApostrophe from './fixtures/status-with-apostrophe.json';
import group from './fixtures/group-truthsocial.json';

describe('buildHtml', () => {
  it('returns empty for no meta', () => {
    const meta = {};
    const request = new Request('https://gleasonator.com');
    const expected = '';
    expect(buildHtml(meta, request)).toEqual(expected);
  });

  it('renders instance meta', () => {
    const meta = { instance };
    const request = new Request('https://gleasonator.com');
    const html = buildHtml(meta, request);
    expect(html).toContain('<title>Gleasonator</title>');
    expect(html).toContain('<meta property="og:title" content="Gleasonator">');
    expect(html).toContain('<link rel="icon" href="https://gleasonator.com/favicon.png">');
  });

  it('renders an account', () => {
    const meta = { account, instance };
    const request = new Request('https://gleasonator.com/@alex');
    const html = buildHtml(meta, request);
    expect(html).toContain('<title>Alex Gleason (@alex)</title>');
    expect(html).toContain('<meta property="og:url" content="https://gleasonator.com/users/alex">');
    expect(html).toContain('<meta property="og:site_name" content="Gleasonator">');
    expect(html).toContain(
      '<meta property="og:description" content="I create Fediverse software that empowers people online.I&#39;m vegan btwNote: If you have a question for me, please tag me publicly. This gives the opportunity for others to chime in, and bystanders to learn.">',
    );
    expect(html).toContain(
      '<meta property="og:image" content="https://media.gleasonator.com/876a0d19821a0a2fea30841c689ce9166a1ca1521dd06fb6ba27b6eab8a99fcd.png">',
    );
  });

  it('renders a status', () => {
    const meta = { status, instance };
    const request = new Request('https://gleasonator.com/@alex/posts/103874034847713213');
    const html = buildHtml(meta, request);
    expect(html).toContain('<title>Alex Gleason (@alex)</title>');
    expect(html).toContain('<meta property="og:description" content="What is tolerance?">');
    expect(html).toContain('<meta property="og:site_name" content="Gleasonator">');
    expect(html).toContain(
      '<meta property="og:image" content="https://media.gleasonator.com/876a0d19821a0a2fea30841c689ce9166a1ca1521dd06fb6ba27b6eab8a99fcd.png">',
    );
    expect(html).toContain(
      '<meta property="og:url" content="https://gleasonator.com/notice/103874034847713213">',
    );
  });

  it('renders media attachment for a status', () => {
    const meta = { status: statusWithImage, instance };
    const request = new Request('https://gleasonator.com/@alex/posts/AFx4KoRHzDmPge7FUu');
    const html = buildHtml(meta, request);
    expect(html).toContain('<meta property="og:image:width" content="1342">');
    expect(html).toContain('<meta property="og:image:height" content="704">');
    expect(html).toContain(
      '<meta property="og:image" content="https://media.gleasonator.com/074862413a7fe1d18a2bee0f626280c8f795b392e9400419cc34ca4f3cb560ee.png">',
    );
  });

  it('renders a group', () => {
    const meta = { group, instance };
    const request = new Request('https://truthsocial.com/groups/109989480368015378');
    const html = buildHtml(meta, request);
    expect(html).toContain('<title>PATRIOT PATRIOTS</title>');
    expect(html).toContain(
      '<meta property="og:url" content="https://covfefe.social/groups/109989480368015378">',
    );
    expect(html).toContain('<meta property="og:description" content="patriots 900000001">');
    expect(html).toContain(
      '<meta property="og:image" content="https://media.covfefe.social/groups/avatars/109/989/480/368/015/378/original/50b0d899bc5aae13.jpg">',
    );
  });

  it('renders a group from slug', () => {
    const meta = { group, instance };
    const request = new Request('https://truthsocial.com/group/patriot-patriots');
    const html = buildHtml(meta, request);
    expect(html).toContain('<title>PATRIOT PATRIOTS</title>');
    expect(html).toContain(
      '<meta property="og:url" content="https://covfefe.social/groups/109989480368015378">',
    );
    expect(html).toContain('<meta property="og:description" content="patriots 900000001">');
    expect(html).toContain(
      '<meta property="og:image" content="https://media.covfefe.social/groups/avatars/109/989/480/368/015/378/original/50b0d899bc5aae13.jpg">',
    );
  });

  it("doesn't double-unescape status content", () => {
    const meta = { status: statusWithApostrophe, instance };
    const request = new Request('https://truthsocial.com/@Qanon76/posts/108754495216677799');
    const html = buildHtml(meta, request);
    expect(html).toContain('&#39;He ');
    expect(html).not.toContain('&amp;apos;');
  });

  it('returns fallback og:url and extra metadata', () => {
    const meta = { instance };
    const request = new Request('https://gleasonator.com/developers');
    const html = buildHtml(meta, request);
    expect(html).toContain('<meta property="og:url" content="https://gleasonator.com/developers">');
    expect(html).toContain('<meta property="og:site_name" content="Gleasonator">');
    expect(html).toContain('<meta property="og:type" content="website">');
    expect(html).toContain('<meta name="twitter:card" content="summary">');
  });

  it('sanitizes markup from the API', () => {
    const meta = { account, instance };
    const request = new Request('https://gleasonator.com/@alex');
    const html = buildHtml(meta, request);
    expect(html).not.toContain('<br/>');
    expect(html).toContain('I&#39;m vegan btw');
  });
});
