import { buildConfig } from '../src/config';

describe('buildConfig', () => {
  let warn: jest.SpyInstance;
  beforeAll(() => (warn = jest.spyOn(console, 'warn').mockImplementation()));
  afterAll(() => warn.mockReset());

  it('returns default config', () => {
    const config = buildConfig({});
    expect(config.crawlerRegex.source).toContain('mastodon|pleroma');
    expect(config.apiCacheTtl).toBe(300);
  });

  it('parses custom crawler regex', () => {
    const config = buildConfig({ CRAWLER_REGEX: 'gleasonator|benis' });
    expect(config.crawlerRegex.source).toEqual('gleasonator|benis');
    expect(config.crawlerRegex.flags).toEqual('i');
  });

  it('warns and falls back with invalid regex', () => {
    const config = buildConfig({ CRAWLER_REGEX: '???' });
    expect(config.crawlerRegex.source).toContain('mastodon|pleroma');
    expect(config.crawlerRegex.flags).toEqual('i');
    expect(warn).toBeCalledWith('Could not parse RegExp from configuration!: ???');
  });

  it('parses custom number', () => {
    const config = buildConfig({ API_CACHE_TTL: '3600' });
    expect(config.apiCacheTtl).toEqual(3600);
  });

  it('warns and falls back with invalid number', () => {
    const config = buildConfig({ API_CACHE_TTL: '???' });
    expect(config.apiCacheTtl).toEqual(300);
    expect(warn).toBeCalledWith('Could not parse number from configuration!: ???');
  });
});
