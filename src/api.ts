import Config from './config';

import type { Instance, Account, Status, Group } from './types';

/** GET utility function to fetch JSON from the API. */
const get = async <T>(event: FetchEvent, path: string): Promise<T | undefined> => {
  const cache = await caches.open('soapbox:api');
  const { origin } = new URL(event.request.url);
  const url = origin + path;
  const cachedResp = await cache.match(url);

  // If cache exists, serve immediately.
  if (cachedResp) {
    return cachedResp.json();
  }

  // If the user is a crawler, wait for the API.
  if (isCrawler(event)) {
    return (await fetchAndCache(event, url)).json();
  }
};

/**
 * Fetch, and pass real IP to upstream as `cf-connecting-ip`.
 * Bizarrely, Cloudflare rewrites `X-Real-Ip` to `cf-connecting-ip`
 * and it's the only possible header we can send upstream!
 * @see {@link https://community.cloudflare.com/t/will-the-header-x-real-ip-always-be-renamed-to-cf-connecting-ip-in-fetch-headers/120532/5}
 */
const realIpFetch = async (event: FetchEvent, url: string): Promise<Response> => {
  const ip = event.request.headers.get('cf-connecting-ip');

  const headers: HeadersInit = {
    'user-agent': 'soapbox-worker',
  };

  if (ip) {
    headers['x-real-ip'] = ip;
  }

  return fetch(url, { headers });
};

/** Check if the request comes from a link crawler. */
const isCrawler = (event: FetchEvent): boolean => {
  const agent = event.request.headers.get('user-agent');

  if (agent) {
    return Config.crawlerRegex.test(agent);
  } else {
    return false;
  }
};

/** Fetch and store to cache . */
const fetchAndCache = async (event: FetchEvent, url: string): Promise<Response> => {
  const resp = await realIpFetch(event, url);

  if (resp.ok) {
    const cache = await caches.open('soapbox:api');
    const clone = new Response(resp.body, {
      headers: { 'cache-control': `public, max-age=${Config.apiCacheTtl}` },
    });
    const result = clone.clone();
    await cache.put(url, clone);
    return result;
  } else {
    throw resp;
  }
};

/** Fetch the instance from the API. */
const fetchInstance = async (event: FetchEvent): Promise<Instance | undefined> => {
  return get(event, '/api/v1/instance');
};

/** Fetch an account by acct from the API. */
const accountLookup = async (event: FetchEvent, acct: string): Promise<Account | undefined> => {
  return get(event, `/api/v1/accounts/lookup?acct=${acct}`);
};

/** Fetch a status by acct from the API. */
const fetchStatus = async (event: FetchEvent, statusId: string): Promise<Status | undefined> => {
  return get(event, `/api/v1/statuses/${statusId}`);
};

/** Fetch a group by ID from the API. */
const fetchGroup = async (event: FetchEvent, groupId: string): Promise<Group | undefined> => {
  return get(event, `/api/v1/groups/${groupId}`);
};

/** Fetch a group by its slug from the API. */
const groupLookup = async (event: FetchEvent, groupSlug: string): Promise<Group | undefined> => {
  return get(event, `/api/v1/groups/lookup?name=${groupSlug}`);
};

export { fetchInstance, accountLookup, fetchStatus, fetchGroup, groupLookup };
