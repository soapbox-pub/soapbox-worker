/* eslint-disable no-var */
export {};

declare global {
  /** How long to cache API responses in seconds. */
  var API_CACHE_TTL: string | undefined;
  /** User-Agent regex for known link crawlers. */
  var CRAWLER_REGEX: string | undefined;
}
