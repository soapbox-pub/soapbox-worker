import merge from 'lodash/merge';

interface GlobalConfig {
  /** How long to cache API responses in seconds. */
  API_CACHE_TTL?: string;
  /** User-Agent regex for known link crawlers. */
  CRAWLER_REGEX?: string;
}

interface IConfig {
  /** Regex to identify link crawlers. */
  crawlerRegex: RegExp;
  /** How long to cache API responses in seconds. */
  apiCacheTtl: number;
}

/** Default configuration. */
const defaults: IConfig = {
  crawlerRegex: new RegExp(
    'googlebot|bingbot|yandex|baiduspider|twitterbot|facebookexternalhit|rogerbot|linkedinbot|embedly|quora link preview|showyoubot|outbrain|pinterestbot|slackbot|vkShare|W3C_Validator|whatsapp|mastodon|pleroma',
    'i',
  ),
  apiCacheTtl: 300,
};

/** Parse a Regex from a string. */
const parseRegExp = (pattern: string | undefined, flags: string): RegExp | undefined => {
  if (pattern) {
    try {
      return new RegExp(pattern, flags);
    } catch (e) {
      console.warn(`Could not parse RegExp from configuration!: ${pattern}`);
      return undefined;
    }
  }
};

/** Parse string to number, dropping NaN. */
const parseNumber = (number: string | undefined): number | undefined => {
  if (number) {
    const result = parseInt(number);
    if (!isNaN(result)) {
      return result;
    } else {
      console.warn(`Could not parse number from configuration!: ${number}`);
      return undefined;
    }
  }
};

/**
 * Build configuration for the worker.
 * This is a separate function so we can run it in tests.
 */
const buildConfig = (config: GlobalConfig): IConfig => {
  const env = {
    crawlerRegex: parseRegExp(config.CRAWLER_REGEX, 'i'),
    apiCacheTtl: parseNumber(config.API_CACHE_TTL),
  };

  return merge({ ...defaults }, env);
};

/** Worker configuration. */
const Config: IConfig = buildConfig(globalThis);

export { Config as default, buildConfig };
