/** Information derived from URL paths. */
interface PathParams {
  statusId?: string;
  acct?: string;
  groupId?: string;
  groupSlug?: string;
}

/** Instance entity from the API. */
interface Instance {
  title: string;
  description: string;
  short_description?: string;
  thumbnail: string;
  pleroma?: {
    favicon?: string;
  };
}

/** Account entity from the API. */
interface Account {
  acct: string;
  display_name: string;
  note: string;
  avatar_static: string;
  url: string;
}

/** Attachment entity from the API. */
interface Attachment {
  type: string;
  meta?: {
    original?: {
      width?: number;
      height?: number;
    };
  };
  preview_url?: string;
  url?: string;
}

/** Status entity from the API. */
interface Status {
  content: string;
  account: Account;
  media_attachments?: Attachment[] | null;
  url: string;
}

/** Group entity from the API. */
interface Group {
  avatar: string;
  note: string;
  display_name: string;
  url: string;
}

/** List of possible fetched entities from the API. */
interface Meta {
  status?: Status;
  account?: Account;
  instance?: Instance;
  group?: Group;
}

/** Width and height in pixels. */
interface Dimensions {
  width: number;
  height: number;
}

export type { PathParams, Instance, Account, Status, Group, Attachment, Dimensions, Meta };
