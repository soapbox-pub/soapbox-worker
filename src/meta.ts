import escape from 'lodash/escape';
import unescape from 'lodash/unescape';

import { fetchInstance, accountLookup, fetchStatus, fetchGroup, groupLookup } from './api';

import type { PathParams, Meta, Attachment, Dimensions } from './types';

/** Fetch meta from params. */
const fetchMeta = async (event: FetchEvent, params: PathParams): Promise<Meta> => {
  // Fetch API async.
  const [instance, account, status, group] = await Promise.all([
    // Always fetch instance.
    fetchInstance(event).catch((error) => {
      console.error(error);
      return undefined;
    }),

    // Only fetch account if not fetching status.
    params.acct && !params.statusId
      ? accountLookup(event, params.acct).catch((error) => {
          console.error(error);
          return undefined;
        })
      : undefined,

    // Fetch status if a statusId is provided.
    params.statusId
      ? fetchStatus(event, params.statusId).catch((error) => {
          console.error(error);
          return undefined;
        })
      : undefined,

    // Fetch group from groupId or groupSlug, if provided.
    params.groupId
      ? fetchGroup(event, params.groupId).catch((error) => {
          console.error(error);
          return undefined;
        })
      : params.groupSlug
      ? groupLookup(event, params.groupSlug).catch((error) => {
          console.error(error);
          return undefined;
        })
      : undefined,
  ]);

  return {
    instance,
    account,
    status,
    group,
  };
};

/** Convert meta into an HTML string. */
const buildHtml = (meta: Meta, request: Request): string => {
  const tags: string[] = [];

  const title = sanitize(getTitle(meta));
  const favicon = sanitize(getFavicon(meta));
  const description = sanitize(getDescription(meta));
  const image = sanitize(getImage(meta));
  const url = sanitize(getUrl(meta, request));
  const siteName = sanitize(meta.instance?.title);
  const dimensions = getDimensions(meta);

  if (title) {
    tags.push(`<title>${title}</title>`);
    tags.push(`<meta property="og:title" content="${title}">`);
    tags.push(`<meta name="twitter:title" content="${title}">`);
  }

  if (favicon) {
    tags.push(`<link rel="icon" href="${favicon}">`);
  }

  if (description) {
    tags.push(`<meta name="description" content="${description}">`);
    tags.push(`<meta property="og:description" content="${description}">`);
    tags.push(`<meta name="twitter:description" content="${description}">`);
  }

  if (image) {
    tags.push(`<meta property="og:image" content="${image}">`);
    tags.push(`<meta name="twitter:image" content="${image}">`);
  }

  if (dimensions) {
    tags.push(`<meta property="og:image:width" content="${dimensions.width}">`);
    tags.push(`<meta property="og:image:height" content="${dimensions.height}">`);
  }

  if (siteName) {
    tags.push(`<meta property="og:site_name" content="${siteName}">`);
  }

  // Extra tags (always present if other tags exist).
  if (tags.length > 0) {
    tags.push(`<meta property="og:url" content="${url}">`);
    tags.push('<meta property="og:type" content="website">');
    tags.push('<meta name="twitter:card" content="summary">');
  }

  return tags.join('');
};

/** Get page title from meta. */
const getTitle = (meta: Meta): string | undefined => {
  const account = meta.status?.account || meta.account;
  return account
    ? `${account.display_name} (@${account.acct})`
    : meta.group?.display_name || meta.instance?.title;
};

/** Get site favicon from meta. */
const getFavicon = (meta: Meta): string | undefined => {
  return meta.instance?.pleroma?.favicon;
};

/** Get page description from meta. */
const getDescription = (meta: Meta): string | undefined => {
  const account = meta.status?.account || meta.account;
  return (
    meta.status?.content ||
    account?.note ||
    meta.group?.note ||
    meta.instance?.short_description ||
    meta.instance?.description
  );
};

/** Get page preview image from meta. */
const getImage = (meta: Meta): string | undefined => {
  const account = meta.status?.account || meta.account;
  const attachment = getAttachment(meta);
  return (
    attachment?.preview_url ||
    account?.avatar_static ||
    meta.group?.avatar ||
    meta.instance?.thumbnail
  );
};

/** Get page URL from meta. */
const getUrl = (meta: Meta, request: Request): string => {
  const account = meta.status?.account || meta.account;
  return meta.status?.url || account?.url || meta.group?.url || request.url;
};

/** Get first image from status, if any. */
const getAttachment = (meta: Meta): Attachment | undefined => {
  return meta.status?.media_attachments?.find((a) => a.type === 'image');
};

/** Get dimensions of first attachment in status, if any. */
const getDimensions = (meta: Meta): Dimensions | undefined => {
  const attachment = getAttachment(meta);
  const width = attachment?.meta?.original?.width;
  const height = attachment?.meta?.original?.height;

  if (width && height && typeof width === 'number' && typeof height === 'number') {
    return { width, height };
  }
};

/**
 * Simple yet insecure tag stripping function.
 * But it's okay because the backend sanitizes it first.
 */
// https://stackoverflow.com/a/822464
const stripTags = (html: string): string => {
  return html.replace(/<[^>]*>?/gi, '');
};

/** Spot-fix certain entities for lodash. */
const fixEntities = (html: string): string => {
  // Lodash doesn't recognize &apos; but it does &#39;
  return html.replaceAll('&apos;', '&#39;');
};

/** Strip tags then escape for safely embedding on the page. */
const sanitize = (html = ''): string => {
  // Escape just in case, but unescape first
  // because the API likely already returns escaped markup.
  return escape(unescape(stripTags(fixEntities(html))));
};

export { fetchMeta, buildHtml };
