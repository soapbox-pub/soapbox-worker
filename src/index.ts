import { match } from 'path-to-regexp';

import { fetchMeta, buildHtml } from './meta';

import type { PathParams } from './types';

/** Placeholder to find & replace with metadata. */
const PLACEHOLDER = '<!--server-generated-meta-->' as const;

/** URL routes to serve metadata on. */
const SSR_ROUTES = [
  '/@:acct/posts/:statusId',
  '/@:acct/:statusId',
  '/@:acct',
  '/users/:acct/statuses/:statusId',
  '/users/:acct',
  '/statuses/:statusId',
  '/groups/:groupId',
  '/groups/:groupId/posts/:statusId',
  '/group/:groupSlug',
  '/notice/:statusId',
  '/embed/:statusId',
] as const;

/** Get path params from a path, if it matches any. */
const getPathParams = (path: string): PathParams => {
  for (const route of SSR_ROUTES) {
    const matcher = match(route, { decode: decodeURIComponent });
    const result = matcher(path);

    if (result) {
      return result.params as PathParams;
    }
  }

  return {};
};

/** Fetch upstream request and inject metadata from path params. */
const fetchAndInject = async (
  event: FetchEvent,
  resp: Response,
  text: string,
  params: PathParams,
): Promise<Response> => {
  const meta = await fetchMeta(event, params);
  const html = buildHtml(meta, event.request);
  const body = text.replace(PLACEHOLDER, html);

  return new Response(body, resp);
};

/** Main handler logic. */
const handleEvent = async (event: FetchEvent): Promise<Response> => {
  let text = '';
  let shouldInject = false;

  const request = event.request;
  const url = new URL(request.url);
  const params = getPathParams(url.pathname);
  const resp = await fetch(request);
  const clone = resp.clone();

  if (resp.headers.get('content-type') === 'text/html') {
    text = await resp.text();
    shouldInject = text.includes(PLACEHOLDER);
  }

  if (shouldInject) {
    return fetchAndInject(event, resp, text, params);
  } else {
    return clone;
  }
};

/**
 * Check if the request is a Websocket request.
 * We need to bail on WS requests to not interfere with the streaming API.
 */
// https://developers.cloudflare.com/workers/learning/using-websockets/
const isWebsocket = (request: Request): boolean => {
  return request.headers.get('Upgrade') === 'websocket';
};

addEventListener('fetch', (event: FetchEvent): void => {
  if (event.request.method === 'GET' && !isWebsocket(event.request)) {
    event.respondWith(handleEvent(event));
  }
});

export { handleEvent, fetchAndInject, getPathParams };
