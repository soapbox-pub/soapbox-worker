# Soapbox Worker

A Cloudflare [Worker](https://developers.cloudflare.com/workers/) to enable server-side rendering (SSR) in Soapbox.

![Screenshot](screenshot.png)

## Why?

In short, link previews.
When you post a link to your Soapbox site on other social platforms like Facebook and Twitter, we want it to display a nice preview of the content.

If you're running Soapbox behind Pleroma, don't sweat it.
Pleroma already injects the required metadata into Soapbox.
However if you're running Soapbox _atop_ the backend, as is necessary for Mastodon, you will need another solution for link previews.

This script is a Cloudflare Worker than injects metadata into Soapbox depending on the URL, all at the edge.

## How it works

The worker is like an advanced proxy, and it's very fast.
It matches the request URL to known patterns and performs API requests behind the scenes to gather data, builds HTML markup for [Open Graph](https://ogp.me/) tags, and injects it into the response from the origin server before serving it to the client.

API credentials are not required.
Only public API endpoints are requested.
Therefore, this solution won't work if authenticated fetch is enabled, and it will fall back to displaying a generic link preview for private accounts and statuses.

API responses are cached in Cloudflare for 5 minutes (configurable).
For performance, only link crawler user-agents (also configurable) will wait for uncached responses.
Everyone else will get meta tags only if they're already in the cache, but the cache will get prewarmed behind the scenes.

## Deploying to Cloudflare

First, set up your worker in Cloudflare:

1. Visit "Workers" > "Overview".
2. Choose "Create a Service".
3. Ensure "HTTP handler" is selected, then click "Create service". Service name doesn't matter.
4. Click "Quick edit".
5. Download the [latest build of worker.js](https://gitlab.com/soapbox-pub/soapbox-worker/-/jobs/artifacts/develop/raw/dist/worker.js?job=build) and paste in its contents.
6. Hit "Save and Deploy"!
7. Finally, go back, then under "Triggers" in your worker.
8. Click "Add Custom Domain", enter your Soapbox domain, and hit submit.
9. That's it!

To test it, try using `curl` with a crawler user-agent:

```sh
curl -H 'user-agent: mastodon' https://my-soapbox.tld/@alex
```

### Upgrading

To upgrade, repeat steps 4-6.

## Developing locally with Miniflare

Local development is done with [Miniflare](https://miniflare.dev/).

```sh
# Clone the repo
git clone https://gitlab.com/soapbox-pub/soapbox-worker.git
cd soapbox-worker

# Install deps
yarn

# Run local dev server
yarn dev --upstream https://my-soapbox.tld
```

### Deploying your local version

To deploy your local version, just login to Cloudflare and push:

```sh
# Login to Cloudflare with wrangler
# (you only have to do this once)
yarn login

# Deploy
# Replace [NAME] with the slug of your Cloudflare worker
yarn deploy [NAME]
```

## Configuration

Cloudflare Workers support variable bindings.
To set these, go under "Settings" > "Variables" in your worker.
All configuration is optional.

- `API_CACHE_TTL` - time in seconds to preserve cached API responses (default: `300`, or 5 minutes).
- `CRAWLER_REGEX` - a regular expression pattern for known link crawler user-agents (eg `mastodon|pleroma|whatsapp`). Only include a pattern, not flags. It is a case-insensitive regex.

## Real IP

One concern is that your upstream server will rate-limit the Cloudflare Worker when it performs API requests.
To prevent this, Soapbox Worker passes the user's real IP in the `cf-connecting-ip` header.
Your upstream server will have to acknowledge `cf-connecting-ip` as a valid forwarding header.

Unfortunately, it is not possible to pass `X-Real-IP` or `X-Forwarded-For` to the upstream.
This is not customizable, and is a [limitation of Cloudflare Workers](https://community.cloudflare.com/t/will-the-header-x-real-ip-always-be-renamed-to-cf-connecting-ip-in-fetch-headers/120532/5).

## This is too proprietary!

Fair, but Cloudflare is in the process of [open-sourcing the Worker runtime](https://blog.cloudflare.com/workers-open-source-announcement/).
Furthermore, it's built on the [Service Worker API](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorker) which is an open web standard.

Plus there are already OSS projects that could kind of perform this job, including [Miniflare](https://miniflare.dev/) itself.
In short, the future is bright for Cloudflare Workers.

## License

MIT License

Copyright (c) 2022 Soapbox contributors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
